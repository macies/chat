﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Chat
{
    class Chat
    {
        List<ConversationManager> conversations;
        TcpListener listener;
        TcpClient client;

        byte[] receiveBuffer;
        

        int myPort;
        int port = 0;

        const int MINPORT = 4000;
        const int MAXPORT = 60000;

        string myIP;
        string Ip = "";



        public Chat()
        {
            receiveBuffer = new Byte[4];
            conversations = new List<ConversationManager>();
  
            myIP = GetIPAddress();
            for (int i = MINPORT; i < MAXPORT; i++)
            {
                try
                {
                    myPort = i;
                    listener = new TcpListener(IPAddress.Parse(myIP), myPort);
                    listener.Start();
                    break;

                }
                catch (Exception ex)
                { Console.WriteLine(ex.Message); }
            }
       
          
            
            listener.BeginAcceptTcpClient(OnAccept, listener);

        }

        public string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        public string GetPort()
        {
            return myPort.ToString();
        }

        public void Connect(string ip, int port)
        {
            foreach (ConversationManager conversation in conversations)
            {
                if(ip == conversation.IP && port == conversation.Port)
                {
                MessageBox.Show("Taka konwersacja juz istnieje");
                return ;
                }
            }


            IPAddress adress;
            if(!IPAddress.TryParse(ip,out adress))
            {
                MessageBox.Show("Błędny adres IP");
                return ;
            }

            client = new TcpClient();

            try
            {
                Console.WriteLine("Proba laczenia");
                client.Connect(adress, port);
            }
            catch
            {
                MessageBox.Show("Polaczenie nie zostalo nawiazane");
                return;
            }

            Console.WriteLine("Polaczono");

            SendData();
            System.Threading.Thread.Sleep(300);
            conversations.Add(new ConversationManager(ref client,ip,port));

        }

        public void SendMessage(string message, int index)
        {
            conversations[index].SendMessage(message);
        }

        private void OnIPReading(IAsyncResult res)
        {
            char charReceived = Encoding.ASCII.GetString(receiveBuffer, 0, 1)[0];
          
            if (charReceived == '#')
            {
                client.GetStream().BeginRead(receiveBuffer, 0, 4, OnPortReading, client);
            }
            else
            {
                Ip = Ip + charReceived;
                client.GetStream().BeginRead(receiveBuffer, 0,1 , OnIPReading, client);
                
            }
            Console.WriteLine("Otrzymano IP : " + charReceived);
        }

        private void OnPortReading(IAsyncResult res)
        {
            int number = BitConverter.ToInt32(receiveBuffer, 0);
            if (number == -1)
            {
                Console.WriteLine("Otrzymano port : " + port);
            }
            else
            {
                port = port * 10;
                port = port + number;
                client.GetStream().BeginRead(receiveBuffer, 0, 4, OnPortReading, client);
            }
        }

        private void OnAccept(IAsyncResult result)
        {
            client = ((TcpListener)result.AsyncState).EndAcceptTcpClient(result);

            client.GetStream().BeginRead(receiveBuffer, 0, 1, OnIPReading, client); 
            
            System.Threading.Thread.Sleep(100);

            Console.WriteLine("Skonczono czytac");

            client.GetStream().BeginRead(receiveBuffer, 0, 1, OnIPReading, client);
          
            bool flag = true;

            foreach (ConversationManager conversation in conversations)
            {
                if (conversation.Port == port && conversation.IP == Ip)
                {
                    flag = false;
                }
            }
            if (flag)
            {
                client = new TcpClient();
                Console.WriteLine("Adres Ip to : " +Ip);
             
                client.Connect(IPAddress.Parse(Ip), port);

                
                conversations.Add(new ConversationManager(ref client, Ip, port));

            }

            port = 0;
            Ip = "";
            listener.BeginAcceptTcpClient(OnAccept, listener);

        }

        private void SendData()
        {
            NetworkStream stream = client.GetStream();

            byte[] buff = Encoding.ASCII.GetBytes(myIP);
            stream.Write(buff, 0, myIP.Length);

            buff = Encoding.ASCII.GetBytes("#");
            stream.Write(buff, 0, 1);

            Int32 port32 = port;

            buff = BitConverter.GetBytes(port32);
            stream.Write(buff, 0, buff.Length);

            buff = BitConverter.GetBytes(-1);
            stream.Write(buff, 0, buff.Length);

        }

       
    }
}
