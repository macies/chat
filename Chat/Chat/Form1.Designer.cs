﻿namespace Chat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connect_button = new System.Windows.Forms.Button();
            this.send_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IP_label = new System.Windows.Forms.Label();
            this.port_label = new System.Windows.Forms.Label();
            this.message_textBox = new System.Windows.Forms.TextBox();
            this.portValue = new System.Windows.Forms.NumericUpDown();
            this.Chaty = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.IPvalue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.portValue)).BeginInit();
            this.Chaty.SuspendLayout();
            this.SuspendLayout();
            // 
            // connect_button
            // 
            this.connect_button.Location = new System.Drawing.Point(626, 137);
            this.connect_button.Name = "connect_button";
            this.connect_button.Size = new System.Drawing.Size(75, 23);
            this.connect_button.TabIndex = 1;
            this.connect_button.Text = "Polacz";
            this.connect_button.UseVisualStyleBackColor = true;
            this.connect_button.Click += new System.EventHandler(this.button1_Click);
            // 
            // send_button
            // 
            this.send_button.Location = new System.Drawing.Point(530, 348);
            this.send_button.Name = "send_button";
            this.send_button.Size = new System.Drawing.Size(75, 23);
            this.send_button.TabIndex = 2;
            this.send_button.Text = "Wyslij";
            this.send_button.UseVisualStyleBackColor = true;
            this.send_button.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(623, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Twoje IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(623, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Twoj Port";
            // 
            // IP_label
            // 
            this.IP_label.AutoSize = true;
            this.IP_label.Location = new System.Drawing.Point(688, 9);
            this.IP_label.Name = "IP_label";
            this.IP_label.Size = new System.Drawing.Size(35, 13);
            this.IP_label.TabIndex = 5;
            this.IP_label.Text = "label3";
            // 
            // port_label
            // 
            this.port_label.AutoSize = true;
            this.port_label.Location = new System.Drawing.Point(688, 36);
            this.port_label.Name = "port_label";
            this.port_label.Size = new System.Drawing.Size(35, 13);
            this.port_label.TabIndex = 6;
            this.port_label.Text = "label4";
            // 
            // message_textBox
            // 
            this.message_textBox.Location = new System.Drawing.Point(12, 350);
            this.message_textBox.Name = "message_textBox";
            this.message_textBox.Size = new System.Drawing.Size(498, 20);
            this.message_textBox.TabIndex = 7;
            // 
            // portValue
            // 
            this.portValue.Location = new System.Drawing.Point(626, 207);
            this.portValue.Maximum = new decimal(new int[] {
            64000,
            0,
            0,
            0});
            this.portValue.Minimum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.portValue.Name = "portValue";
            this.portValue.Size = new System.Drawing.Size(120, 20);
            this.portValue.TabIndex = 8;
            this.portValue.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // Chaty
            // 
            this.Chaty.Controls.Add(this.tabPage1);
            this.Chaty.Controls.Add(this.tabPage2);
            this.Chaty.Location = new System.Drawing.Point(0, 0);
            this.Chaty.Name = "Chaty";
            this.Chaty.SelectedIndex = 0;
            this.Chaty.Size = new System.Drawing.Size(605, 342);
            this.Chaty.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(597, 316);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(597, 316);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(674, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "port";
            // 
            // IPvalue
            // 
            this.IPvalue.Location = new System.Drawing.Point(626, 265);
            this.IPvalue.Name = "IPvalue";
            this.IPvalue.Size = new System.Drawing.Size(120, 20);
            this.IPvalue.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(674, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "IP";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 382);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IPvalue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Chaty);
            this.Controls.Add(this.portValue);
            this.Controls.Add(this.message_textBox);
            this.Controls.Add(this.port_label);
            this.Controls.Add(this.IP_label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.send_button);
            this.Controls.Add(this.connect_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Chat";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.portValue)).EndInit();
            this.Chaty.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connect_button;
        private System.Windows.Forms.Button send_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label IP_label;
        private System.Windows.Forms.Label port_label;
        private System.Windows.Forms.TextBox message_textBox;
        private System.Windows.Forms.NumericUpDown portValue;
        private System.Windows.Forms.TabControl Chaty;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox IPvalue;
        private System.Windows.Forms.Label label4;
    }
}

