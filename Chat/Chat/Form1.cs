﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chat
{
    public partial class Form1 : Form
    {
        Chat chat;
        public Form1()
        {
            InitializeComponent();
            chat = new Chat (Chaty);

            IP_label.Text = chat.GetIPAddress();
            port_label.Text = chat.GetPort();

            Chaty.TabPages.Remove(tabPage1);
            Chaty.TabPages.Remove(tabPage2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chat.Connect(IPvalue.Text, (int)portValue.Value);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string message = message_textBox.Text;
            int id = Chaty.SelectedIndex;
            chat.SendMessage(message, id);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            chat.ShutDown();
        }
    }
}
