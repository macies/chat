﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Chat
{
    class ConversationManager
    {
        TcpClient client;
        NetworkStream stream;
        Thread thread;
        TextBox box;

        int port;
        string Ip;

        bool sendFlag = false;
        public bool working = true;

        byte[] sendBuffer;
        byte[] receiveBuffer;

        string messageReceived = "";

        
        public int Port { get { return port; } }
        public string IP { get { return Ip; } }



        public ConversationManager(TcpClient cl, string ip, int PORT,TextBox Box)
        {

            Ip = ip;
            port = PORT;
            client = cl;

            box = Box;

            receiveBuffer = new Byte[1];
            receiveBuffer[0] = 0;
            
            stream = client.GetStream();
            stream.ReadTimeout = 400;
            stream.WriteTimeout = 1000000;

            
            Read();
        }

        public void SendMessage(string message)
        {
            string text = message + (char)(0); // znak nowego wiersza
            sendBuffer = Encoding.ASCII.GetBytes(text);
            Console.WriteLine("Wysylam wiadomosc :" + Encoding.ASCII.GetString(sendBuffer) + " o wielkosci :" + sendBuffer.Length + " do IP " + Ip + " na port : " + port);
            Send(message);

        }

        private void Send(string message)
        {
            sendFlag = true;
            Thread.Sleep(100);
            stream.BeginWrite(sendBuffer, 0, sendBuffer.Length, OnWrite, message);

        }

        private void Read()
        {
            Console.WriteLine("Rozpoczynam czytac");
            thread = new Thread(() => { StartReading(); });
            thread.Start();
        }

        private void StartReading()
        {
            while (working)
            {
                System.Threading.Thread.Sleep(50);
                if (!sendFlag && working == true)
                {
                    
                    if (stream.DataAvailable)
                    {
                        stream.Read(receiveBuffer, 0, 1);
                        char charReceived = Encoding.ASCII.GetString(receiveBuffer)[0];
                        messageReceived += charReceived;

                        if(charReceived == (char)(0))
                        {
                           
                            box.Invoke((MethodInvoker)(() => 
                                 {
                                     box.AppendText(Ip + " : " + messageReceived);
                                     box.AppendText(Environment.NewLine);
                                 }
                            ));
                            Console.WriteLine(Ip + " : " + messageReceived);
                            messageReceived = "";

                        }

                    }
                }
                
            }

        }

        private void OnWrite(IAsyncResult res)
        {
            
            box.Invoke((MethodInvoker) (() => { box.AppendText("You : " + (string)res.AsyncState + Environment.NewLine); }));

            Console.WriteLine("You : " + (string)res.AsyncState);

            sendFlag = false;
        }

    }
}

