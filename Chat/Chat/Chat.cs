﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Chat
{
    class Chat
    {
        List<ConversationManager> conversations;
        TcpListener listener;
        TcpClient client;
        TabControl tab;


        byte[] receiveBuffer;

        int ConnectionsReceived = 1;
        int Connections = 1;

        int myPort;
        int Port = 0;

        const int MINPORT = 4000;
        const int MAXPORT = 60000;

        string myIP;
        string Ip = "";



        public Chat(TabControl control)
        {
            tab= control;
            
            receiveBuffer = new Byte[4];
            conversations = new List<ConversationManager>();
  
            myIP = GetIPAddress();
            for (int i = MINPORT; i < MAXPORT; i++)
            {
                try
                {
                    myPort = i;
                    listener = new TcpListener(IPAddress.Parse(myIP), myPort);
                    listener.Start();
                    break;

                }
                catch (Exception ex)
                { Console.WriteLine(ex.Message); }
            }
       
          
            
            listener.BeginAcceptTcpClient(OnAccept, listener);

        }

        public string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        public string GetPort()
        {
            return myPort.ToString();
        }

        public void Connect(string ip, int port)
        {
            Ip = ip;
            Port = port;

            Console.WriteLine("Laczenie nr " + Connections);
            Connections++;
            Console.WriteLine(conversations.Count + "TYLE");
            foreach (ConversationManager conversation in conversations)
            {
                if(ip == conversation.IP && port == conversation.Port)
                {
                MessageBox.Show("Taka konwersacja juz istnieje");
                return ;
                }
            }

            Console.WriteLine(conversations.Count + "TYLE");
            IPAddress adress;
            if(!IPAddress.TryParse(ip,out adress))
            {
                MessageBox.Show("Błędny adres IP");
                return ;
            }

            client = new TcpClient();
            Console.WriteLine(conversations.Count + "TYLE");
            try
            {
                Console.WriteLine("Proba laczenia");
                client.Connect(adress, port);
            }
            catch
            {
                MessageBox.Show("Polaczenie nie zostalo nawiazane");
                return;
            }
            Console.WriteLine(conversations.Count + "TYLE");
            Console.WriteLine("Polaczono");

            AddNewConversation();



            SendData();
            Console.WriteLine(conversations.Count + "TYLE");
            System.Threading.Thread.Sleep(300);
            
            Console.WriteLine("1Jest juz konwersacji :" + conversations.Count);

        }

        private void AddNewConversation()
        {
            TabPage page = new TabPage(Ip);
            TextBox box = new TextBox();
            page.Controls.Add(box);

            box.Multiline = true;
            box.ScrollBars = ScrollBars.Vertical;
            box.Dock = DockStyle.Fill;


            tab.Invoke((MethodInvoker)(() => { tab.TabPages.Add(page); }));
            conversations.Add(new ConversationManager(client, Ip, Port, box));
           
        }

        public void  SendMessage(string message, int index)
        {
           conversations[index].SendMessage(message);
        }

        public void ShutDown()
        {
            foreach( ConversationManager conversation in conversations)
            {

                conversation.working = false;
            }

        }

        private void OnIPReading(IAsyncResult res)
        {
            Console.WriteLine("Czekam na kolejny znak");
            char charReceived = Encoding.ASCII.GetString(receiveBuffer, 0, 1)[0];
            Console.WriteLine("Otrzymano znak IP = " + charReceived);
            if (charReceived == '#')
            {
                client.GetStream().BeginRead(receiveBuffer, 0, 4, OnPortReading, client);
            }
            else
            {
                Ip = Ip + charReceived;
                try
                {
                    client.GetStream().BeginRead(receiveBuffer, 0, 1, OnIPReading, client);
                } catch 
                {
                    Console.WriteLine("Przerwano rozmowe");
                }
            }
       
        }

        private void OnPortReading(IAsyncResult res)
        {
            int number = BitConverter.ToInt32(receiveBuffer, 0);
            Console.WriteLine(number + " to otrzymana cyfra");
            if (number == -1)
            {
                return;
            }
            else
            {
                Port = Port * 10;
                Port = Port + number;
                client.GetStream().BeginRead(receiveBuffer, 0, 4, OnPortReading, client);
            }
        }

        private void OnAccept(IAsyncResult result)
        {
            Console.WriteLine("Obsługuję połączanie nr " + ConnectionsReceived);
            ConnectionsReceived++;

            client = ((TcpListener)result.AsyncState).EndAcceptTcpClient(result);

            client.GetStream().BeginRead(receiveBuffer, 0, 1, OnIPReading, client); 
            
            

            Console.WriteLine("Skonczono czytac");
            Console.WriteLine("Adres Ip to : " + Ip);
            Console.WriteLine("Port to : " + Port);

            bool flag = true;

            foreach (ConversationManager conversation in conversations)
            {
                Console.WriteLine("Porownuje IP :" + Ip + " i " + conversation.IP);
                Console.WriteLine("Porownuje porty :" + Port + " i " + conversation.Port);

                if (conversation.Port == Port && conversation.IP == Ip)
                {
                    flag = false;
                }
            }
            if (flag)
            {
                AddNewConversation();
                Console.WriteLine("Jest juz konwersacji :" + conversations.Count);
            }

            Port = 0;
            Ip = "";
            listener.BeginAcceptTcpClient(OnAccept, listener);

        }

        private void SendData()
        {
            NetworkStream stream = client.GetStream();

            byte[] buff = Encoding.ASCII.GetBytes(myIP);
            stream.Write(buff, 0, myIP.Length);

            buff = Encoding.ASCII.GetBytes("#");
            stream.Write(buff, 0, 1);

            Int32 port32 = myPort;
            Console.WriteLine("Wysyłam port : " + port32);
            buff = BitConverter.GetBytes(port32);
            stream.Write(buff, 0, buff.Length);

            buff = BitConverter.GetBytes(-1);
            stream.Write(buff, 0, buff.Length);

        }

       
    }
}
